package com.rejitsoft.prime_factorization;

import com.rejitsoft.prime_factorization.factorization.IFactorization;
import com.rejitsoft.prime_factorization.types.BaseNumberType;
import com.rejitsoft.prime_factorization.types.TypeOnBigInt;
import com.rejitsoft.prime_factorization.types.TypeOnUnsignedLong;

import junit.framework.Assert;

import org.junit.Test;

public class JavaPrimesHelperUnitTest
{

  abstract class TestCallback extends IFactorization.Callback
  {
    public void onStart() { }
    public void onProgress(int progress, int total)  { }
    public void onError(String error)  { }
  };

  public void checkPrimes(String strInput, final String[] strCheck, BaseNumberType.Factory factory)
  {
    BaseNumberType primeInput = factory.create(strInput);
    int i = 0;
    JavaPrimesHelper.primeFactors(primeInput, new TestCallback()
    {

      @Override
      public void onSuccess(String[] result)
      {
        Assert.assertTrue(result.length == strCheck.length);
        int i = 0;
        for (String res : result)
        {
          Assert.assertEquals(strCheck[i++], res);
        }
      }
    });
  }

  public static void checkPrimesFactorizationFromFactroy(BaseNumberType.Factory factory)
  {
    JavaPrimesHelperUnitTest test = new JavaPrimesHelperUnitTest();
    test.checkPrimes("1565456", new String[]{"2", "2", "2", "2", "97841"}, factory);
    test.checkPrimes("6548791", new String[]{"17", "385223"}, factory);
    test.checkPrimes("9007199254740997", new String[]{"9007199254740997"}, factory);
    test.checkPrimes("9007199254740881", new String[]{"9007199254740881"}, factory);
    test.checkPrimes("9999986200004761", new String[] {"99999931", "99999931"}, factory);
  }

  @Test
  public void checkBigIntPrimesFactorization() throws Exception
  {
    checkPrimesFactorizationFromFactroy(new TypeOnBigInt.Factory());
  }

  @Test
  public void checkUnsignedLongPrimesFactorization() throws Exception
  {
    checkPrimesFactorizationFromFactroy(new TypeOnUnsignedLong.Factory());
  }

}