package com.rejitsoft.prime_factorization;

import com.rejitsoft.prime_factorization.factorization.IFactorization;

public class NativePrimeFactorization
{
  public native static String[] primeFactors(String number, IFactorization.Callback callback);
}
