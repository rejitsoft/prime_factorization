package com.rejitsoft.prime_factorization;


import com.rejitsoft.prime_factorization.factorization.IFactorization;
import com.rejitsoft.prime_factorization.types.BaseNumberType;

import java.util.ArrayList;
import java.util.List;

public class JavaPrimesHelper
{
  private static int TOTAL_PROGRESS = 100;

  public static void primeFactors(BaseNumberType numbers, IFactorization.Callback callback)
  {
    BaseNumberType n = numbers;
    BaseNumberType ZERRO = numbers.getFactory().create("0");
    BaseNumberType ONE = numbers.getFactory().create("1");
    BaseNumberType TWO = numbers.getFactory().create("2");
    BaseNumberType BN_TOTAL_PROGRESS = numbers.getFactory().create("100");
    List<BaseNumberType> factors = new ArrayList<BaseNumberType>();
    int progress = 0;
    int last_progress = 0;
    BaseNumberType i = TWO;
    BaseNumberType limit = n.divide(i);
    BaseNumberType one_percent;
    try
    {
      for (; i.compareTo(limit) <= 0; i = i.add(ONE))
      {
        if (callback != null && callback.isCanceled())
        {
          callback.onCancel();
          return;
        }

        limit = n.divide(i);
        one_percent = (limit.compareTo(BN_TOTAL_PROGRESS) > 0 ? limit.divide(BN_TOTAL_PROGRESS) : ONE);
        progress = (int) i.divide(one_percent).toLong();
        if (callback != null && last_progress < progress)
        {
          last_progress = progress;
          callback.onProgress(progress, 100);
        }

        while (n.mod(i).toLong() == 0)
        {
          factors.add(i);
          n = n.divide(i);

          limit = n.divide(i);
          one_percent = (limit.compareTo(BN_TOTAL_PROGRESS) > 0 ? limit.divide(BN_TOTAL_PROGRESS) : ONE);
          if (callback != null && n.compareTo(i) >= 0 && last_progress < progress)
          {
            progress = (int) i.divide(one_percent).toLong();
            last_progress = progress;
            callback.onProgress(progress, TOTAL_PROGRESS);
          }
        }
      }
      if (n.compareTo(ONE) > 0)
      {
        factors.add(n);
      }

      if (callback != null && progress != TOTAL_PROGRESS)
        callback.onProgress(TOTAL_PROGRESS, TOTAL_PROGRESS);
    }
    catch (Exception e)
    {
      if (callback != null)
      {
        callback.onError(e.toString());
      }
    }

    if (callback != null)
    {
      String[] stringResult = new String[factors.size()];
      int j = 0;
      for (BaseNumberType number: factors)
      {
        stringResult[j++] = number.toString();
      }

      callback.onSuccess(stringResult);
    }
  }
}
