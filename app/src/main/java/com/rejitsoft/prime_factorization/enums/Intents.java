package com.rejitsoft.prime_factorization.enums;


public class Intents
{
  public static final String I_CALC_PROGRESS = "I_CALC_PROGRESS";
  public static final String E_CALC_PROGRESS = "E_CALC_PROGRESS";
  public static final String I_CALC_RESULT = "I_CALC_RESULT";
  public static final String E_CALC_RESULT = "E_CALC_RESULT";
  public static final String I_CALC_ERROR = "I_CALC_ERROR";
  public static final String E_CALC_ERROR = "E_CALC_ERROR";
  public static final String I_CALC_CANCEL = "I_CALC_CANCEL";
  public static final String E_CALC_CANCEL = "E_CALC_CANCEL";
}
