package com.rejitsoft.prime_factorization;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ServiceCommunicateHelper
{
  private Messenger mService = null;
  private boolean isBoundToService = false;
  private Context mContext;
  private List<Message> msgQueue = new ArrayList<Message>();

  public ServiceCommunicateHelper(Context context)
  {
    mContext = context;
  }

  protected void finalize() throws Throwable
  {
    try
    {
      unbindService();
    }
    finally
    {
      super.finalize();
    }
  }

  /**
   * Defines callbacks for service binding, passed to bindService()
   */
  private ServiceConnection mConnection = new ServiceConnection()
  {

    @Override
    public void onServiceConnected(ComponentName className,
                                   IBinder service)
    {
      synchronized (this)
      {
        Log.d(getClass().getName(), "onServiceConnected");
        mService = new Messenger(service);
        isBoundToService = true;
        List<Message> toRemove = new ArrayList<Message>();
        for (Message msq : msgQueue)
        {
          if (sendMsgToService(msq.what, msq.getData()))
            toRemove.add(msq);
        }

        msgQueue.removeAll(toRemove);
      }
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0)
    {
      Log.d(getClass().getName(), "onServiceDisconnected");
      mService = null;
      isBoundToService = false;
    }
  };

  public void bindService(Class objClass)
  {
    Intent intent = new Intent(mContext, objClass);
    mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
  }

  public boolean sendMsgToService(int msgID, Bundle data)
  {
    synchronized (this)
    {
      // Create and send a message to the service, using a supported 'what' value
      Message msg = Message.obtain(null, msgID, 0, 0);
      if (data != null)
        msg.setData(data);

      if (!isBoundToService || mService == null)
      {
        msgQueue.add(msg);
        Log.d(getClass().getName(), "fail sendMsgToService. add to queue " + String.valueOf(msg.what));
        return false;
      }

      try
      {
        mService.send(msg);
        Log.d(getClass().getName(), "sendMsgToService " + String.valueOf(msg.what));
        return true;
      }
      catch (RemoteException e)
      {
      }

      return false;
    }
  }

  public void unbindService()
  {
    if (isBoundToService)
    {
      mContext.unbindService(mConnection);
      isBoundToService = false;
      Log.d(getClass().getName(), "unbindService");
    }
  }

}
