package com.rejitsoft.prime_factorization.types;

import com.google.common.primitives.UnsignedLong;

import java.math.BigInteger;

public class TypeOnUnsignedLong extends BaseNumberType
{
  private UnsignedLong member;

  public static class Factory implements BaseNumberType.Factory
  {
    @Override
    public BaseNumberType create(String v)
    {
      return new TypeOnUnsignedLong(v);
    }
  };

  public TypeOnUnsignedLong(String v)
  {
    member = UnsignedLong.valueOf(v);
  }

  public TypeOnUnsignedLong(UnsignedLong v)
  {
    member = v;
  }

  UnsignedLong getMember()
  {
    return member;
  }

  @Override
  public double toDouble()
  {
    return member.doubleValue();
  }

  @Override
  public long toLong()
  {
    return member.longValue();
  }

  @Override
  public String toString()
  {
    return member.toString();
  }

  @Override
  public BigInteger toBigInt()
  {
    return member.bigIntegerValue();
  }

  @Override
  public BaseNumberType mul(BaseNumberType with)
  {
    return new TypeOnUnsignedLong(member.times(((TypeOnUnsignedLong) with).getMember()));
  }

  @Override
  public BaseNumberType divide(BaseNumberType with)
  {
    return new TypeOnUnsignedLong(member.dividedBy(((TypeOnUnsignedLong)with).getMember()));
  }

  @Override
  public BaseNumberType add(BaseNumberType with)
  {
    return  new TypeOnUnsignedLong(member.plus(((TypeOnUnsignedLong)with).getMember()));
  }

  @Override
  public BaseNumberType mod(BaseNumberType with)
  {
    return  new TypeOnUnsignedLong(member.mod(((TypeOnUnsignedLong)with).getMember()));
  }

  @Override
  public int compareTo(BaseNumberType with)
  {
    return member.compareTo(((TypeOnUnsignedLong)with).getMember());
  }

  @Override
  public Factory getFactory()
  {
    return new Factory();
  }

}
