package com.rejitsoft.prime_factorization.types;

import java.math.BigInteger;
import java.util.AbstractList;
import java.util.List;

public abstract class BaseNumberType
{
  public interface Factory
  {
    BaseNumberType create(String v);
  }

  public BaseNumberType() {}
  public BaseNumberType(String v) {}
  public abstract double toDouble();
  public abstract long toLong();
  public abstract String toString();
  public abstract BigInteger toBigInt();
  public abstract BaseNumberType mul(BaseNumberType with);
  public abstract BaseNumberType divide(BaseNumberType with);
  public abstract BaseNumberType add(BaseNumberType with);
  public abstract BaseNumberType mod(BaseNumberType with);
  public abstract int compareTo(BaseNumberType with);
  public abstract Factory getFactory();
}
