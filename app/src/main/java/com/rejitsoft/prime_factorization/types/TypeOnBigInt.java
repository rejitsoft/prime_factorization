package com.rejitsoft.prime_factorization.types;


import java.math.BigInteger;

public class TypeOnBigInt extends BaseNumberType
{
  private BigInteger member;

  public static class Factory implements BaseNumberType.Factory
  {
    @Override
    public BaseNumberType create(String v)
    {
      return new TypeOnBigInt(v);
    }
  };

  public TypeOnBigInt(String v)
  {
    member = new BigInteger(v);
  }

  public TypeOnBigInt(BigInteger v)
  {
    member = v;
  }

  BigInteger getMember()
  {
    return member;
  }

  @Override
  public double toDouble()
  {
    return member.doubleValue();
  }

  @Override
  public long toLong()
  {
    return member.longValue();
  }

  @Override
  public String toString()
  {
    return member.toString();
  }

  @Override
  public BigInteger toBigInt()
  {
    return member;
  }

  @Override
  public BaseNumberType mul(BaseNumberType with)
  {
    return new TypeOnBigInt(member.multiply(((TypeOnBigInt) with).getMember()));
  }

  @Override
  public BaseNumberType divide(BaseNumberType with)
  {
    return new TypeOnBigInt(member.divide(((TypeOnBigInt) with).getMember()));
  }

  @Override
  public BaseNumberType add(BaseNumberType with)
  {
    return new TypeOnBigInt(member.add(((TypeOnBigInt)with).getMember()));
  }

  @Override
  public BaseNumberType mod(BaseNumberType with)
  {
    return  new TypeOnBigInt(member.mod(((TypeOnBigInt)with).getMember()));
  }

  @Override
  public int compareTo(BaseNumberType with)
  {
    return member.compareTo((((TypeOnBigInt)with).getMember()));
  }

  @Override
  public Factory getFactory()
  {
    return new Factory();
  }
}
