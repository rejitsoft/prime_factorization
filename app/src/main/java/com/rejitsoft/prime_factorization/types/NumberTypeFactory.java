package com.rejitsoft.prime_factorization.types;


import com.google.common.primitives.UnsignedLong;

import java.math.BigInteger;

public class NumberTypeFactory
{
  public static String MAX_PROGRAMM_SUPPORT_NUMBER = "100000000000000000000";
  private static String MAX_UNSIGNED_LONG = UnsignedLong.MAX_VALUE.toString();

  public static boolean checkInputNumber(String imputNumber, String max)
  {
    try
    {
      BigInteger value = new BigInteger(imputNumber);
      if (value.compareTo(new BigInteger(max)) > 0)
      {
        return false;
      }

      return true;
    }
    catch (Exception e)
    {
      return false;
    }
  }

  public static BaseNumberType create(String number)
  {
    if (checkInputNumber(number, MAX_UNSIGNED_LONG))
    {
      return new TypeOnUnsignedLong(number);
    }
    else
    {
      return new TypeOnBigInt(number);
    }
  }
}
