package com.rejitsoft.prime_factorization.factorization;

import com.rejitsoft.prime_factorization.NativePrimeFactorization;

public class CppFactorizationImpl extends AsyncFactorization
{
  @Override
  public void Calculation()
  {
    NativePrimeFactorization.primeFactors(mNumber, mCallback);
  }

  static IFactorization getInstance()
  {
    return new CppFactorizationImpl();
  }
}
