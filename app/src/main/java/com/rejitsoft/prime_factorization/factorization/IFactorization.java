package com.rejitsoft.prime_factorization.factorization;

import java.util.List;

public interface IFactorization
{
  public static abstract class Callback
  {
    private boolean mIsCanceled = false;

    public abstract void onStart();

    public abstract void onProgress(int progress, int total);

    public abstract void onSuccess(String[] result);

    public abstract void onError(String error);

    public void onCancel()
    {
      setCanceled(false);
    }

    public boolean isCanceled()
    {
      synchronized (this)
      {
        return mIsCanceled;
      }
    }

    public void setCanceled(boolean value)
    {
      synchronized (this)
      {
        mIsCanceled = value;
      }
    }
  }

  void Start(String number, Callback callback);

  void Calculation();

  void Cancel();
}
