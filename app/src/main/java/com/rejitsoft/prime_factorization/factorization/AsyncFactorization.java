package com.rejitsoft.prime_factorization.factorization;

import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;

abstract public class AsyncFactorization implements IFactorization
{
  protected AsyncTask<String, Void, Void> mJob;
  protected String mNumber;
  protected Callback mCallback;

  @Override
  public void Start(String number, Callback callback)
  {
    mCallback = callback;
    mNumber = number;
    mJob = new AsyncTask<String, Void, Void>()
    {

      @Override
      protected Void doInBackground(String... params)
      {
        Calculation();
        return null;
      }
    };

    AsyncTaskCompat.executeParallel(mJob, number);
  }

  abstract public void Calculation();

  @Override
  public void Cancel()
  {
    mCallback.setCanceled(true);
  }
}
