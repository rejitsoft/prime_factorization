package com.rejitsoft.prime_factorization.factorization;

import com.rejitsoft.prime_factorization.JavaPrimesHelper;
import com.rejitsoft.prime_factorization.types.NumberTypeFactory;

public class JavaFactorizationImpl extends  AsyncFactorization
{
  @Override
  public void Calculation()
  {
    JavaPrimesHelper.primeFactors(NumberTypeFactory.create(mNumber), mCallback);
  }

  static IFactorization getInstance()
  {
    return new JavaFactorizationImpl();
  }
}
