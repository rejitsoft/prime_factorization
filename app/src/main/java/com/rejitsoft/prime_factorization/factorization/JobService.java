package com.rejitsoft.prime_factorization.factorization;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.rejitsoft.prime_factorization.Preferences;
import com.rejitsoft.prime_factorization.enums.Intents;


public class JobService extends Service
{
  public static final int MSG_CANCEL_JOB = 1;

  private volatile long jobsRunning = 0;
  private boolean isInitialized = false;
  public static String NUMBER = "B_NUMBER";
  public static String ACTION_CPP = "A_CPP";
  public static String ACTION_JAVA = "A_JAVA";
  private LocalBroadcastManager mLocalBR;
  private IFactorization mFactorization;
  final Messenger mMessenger = new Messenger(new IncomingHandler());

  class IncomingHandler extends Handler
  {
    @Override
    public void handleMessage(Message msg)
    {
      switch (msg.what)
      {
        case MSG_CANCEL_JOB:
          if (mFactorization != null)
          {
            mFactorization.Cancel();
          }
          break;
        default:
          super.handleMessage(msg);
      }
    }
  }

  public class JobServiceFactorCallback extends IFactorization.Callback
  {
    @Override
    public void onStart()
    {
      synchronized (this)
      {
        // Check if service was stopped by self
        if (jobsRunning == -1)
          return;

        if (jobsRunning != 0)
        {
          Log.d(getClass().getName(), "Wrong usage: simultaneous startCalc() calls were detected");
          return;
        }

        jobsRunning++;
      }
    }

    @Override
    public void onProgress(int progress, int total)
    {
      mLocalBR.sendBroadcast(new Intent(Intents.I_CALC_PROGRESS).putExtra(Intents.E_CALC_PROGRESS, progress));
    }

    @Override
    public void onSuccess(String[] result)
    {
      String resultStr = prepareStringResult(result);
      Log.d(getClass().getName(), "onSuccess " + resultStr);
      mLocalBR.sendBroadcast(new Intent(Intents.I_CALC_RESULT).putExtra(Intents.E_CALC_RESULT, resultStr));
      Preferences.setString(JobService.this, Preferences.LAST_RESULT, resultStr);
      jobFinished();
    }

    @Override
    public void onError(String error)
    {
      Log.d(getClass().getName(), "onError");
      mLocalBR.sendBroadcast(new Intent(Intents.I_CALC_ERROR).putExtra(Intents.E_CALC_ERROR, error));
      jobFinished();
    }

    @Override
    public void onCancel()
    {
      super.onCancel();
      Log.d(getClass().getName(), "onCancel");
      mLocalBR.sendBroadcast(new Intent(Intents.I_CALC_CANCEL));
      Preferences.setString(JobService.this, Preferences.LAST_RESULT, "");
      jobFinished();
    }
  };

  private IFactorization.Callback callback = new JobServiceFactorCallback();


  public static IntentFilter getIntentFilter()
  {
    IntentFilter filter = new IntentFilter(Intents.I_CALC_PROGRESS);
    filter.addAction(Intents.I_CALC_ERROR);
    filter.addAction(Intents.I_CALC_RESULT);
    filter.addAction(Intents.I_CALC_CANCEL);
    return filter;
  }

  private String prepareStringResult(String[] numbers)
  {
    String result = "";
    int count = 0;
    for (String number : numbers)
    {
      count++;
      result += number;
      if (count != numbers.length)
        result += ", ";
    }

    return result;
  }

  private void jobFinished()
  {
    Preferences.setCalculating(this, false);
    synchronized (JobService.this)
    {
      if (--jobsRunning <= 0)
      {
        jobsRunning = -1;
        JobService.this.stopSelf();
      }
    }
  }

  @Override
  public void onCreate()
  {
    super.onCreate();
    Log.d(getClass().getName(), "onCreate");
    if (!isInitialized)
    {
      mLocalBR = LocalBroadcastManager.getInstance(this);
      isInitialized = true;
    }
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    Log.d(getClass().getName(), "onStartCommand");
    if (intent != null)
      startCalc(intent.getAction(), intent.getExtras().getString(NUMBER));
    return super.onStartCommand(intent, flags, startId);
  }

  private void startCalc(String action, String number)
  {
    if (action == null || action.isEmpty() || number == null || number.isEmpty())
      return;

    if (action.equals(ACTION_CPP))
    {
      mFactorization = CppFactorizationImpl.getInstance();
    }
    else if (action.equals(ACTION_JAVA))
    {
      mFactorization = JavaFactorizationImpl.getInstance();
    }

    mFactorization.Start(number, callback);
  }

  @Override
  public IBinder onBind(Intent intent)
  {
    return mMessenger.getBinder();
  }

  static
  {
    System.loadLibrary("native_prime_factors");
  }
}
