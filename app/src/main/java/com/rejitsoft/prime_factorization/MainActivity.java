package com.rejitsoft.prime_factorization;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.rejitsoft.prime_factorization.enums.Intents;
import com.rejitsoft.prime_factorization.factorization.JobService;
import com.rejitsoft.prime_factorization.types.NumberTypeFactory;


public class MainActivity extends AppCompatActivity
{
  private static final String B_PROGRESS = "B_PROGRESS";
  private TextView mNumbersCount;
  private TextView mResult;
  private EditText mInput;
  private View mProgressView;
  private ProgressBar mProgressBar;
  private TextView mProgressPercent;
  private LocalBroadcastManager mLocalBR;
  private Button mButtonFactorize;
  private RadioButton rbJava;
  private RadioButton rbCpp;
  private ServiceCommunicateHelper serviceHelper;

  private BroadcastReceiver jobServiceBroadcatReciever = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      String action = intent.getAction();
      switch (action)
      {
        case Intents.I_CALC_PROGRESS:
          showProgress(intent.getIntExtra(Intents.E_CALC_PROGRESS, 0));
          break;
        case Intents.I_CALC_RESULT:
          String result = intent.getStringExtra(Intents.E_CALC_RESULT);
          showResult(result);
          finishedCalculation();
          break;
        case Intents.I_CALC_ERROR:
          showError(intent.getStringExtra(Intents.E_CALC_ERROR));
          finishedCalculation();
          break;
        case Intents.I_CALC_CANCEL:
          showCanceled();
          finishedCalculation();
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mNumbersCount = (TextView) findViewById(R.id.tv_numbers_value);
    mResult = (TextView) findViewById(R.id.tv_result);
    mResult.setText(Preferences.getString(MainActivity.this, Preferences.LAST_RESULT));
    mProgressView = findViewById(R.id.progress_bar_layout);
    mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
    mProgressPercent = (TextView) findViewById(R.id.progress_percent);
    rbJava = (RadioButton) findViewById(R.id.rb_java);
    rbCpp = (RadioButton) findViewById(R.id.rb_cpp);
    mButtonFactorize = (Button) findViewById(R.id.btn_factorize);
    mButtonFactorize.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        startOrCancelCalculation();
      }
    });
    mInput = (EditText) findViewById(R.id.et_input_number);
    prepareInputText(mInput);

    serviceHelper = new ServiceCommunicateHelper(this);

    mLocalBR = LocalBroadcastManager.getInstance(this);

    if (savedInstanceState != null)
    {
      if (isCalculating())
      {
        prepareForStart();
        setProgressValue(savedInstanceState.getInt(B_PROGRESS));
      }
      else
      {
        finishedCalculation();
      }
    }
  }

  @Override
  public void onResume()
  {
    super.onResume();
    mLocalBR.registerReceiver(jobServiceBroadcatReciever, JobService.getIntentFilter());
    serviceHelper.bindService(JobService.class);

    if (!isCalculating())
    {
      finishedCalculation();
    }
  }

  @Override
  public void onPause()
  {
    super.onPause();
    mLocalBR.unregisterReceiver(jobServiceBroadcatReciever);
    serviceHelper.unbindService();
  }

  private void prepareInputText(EditText view)
  {
    assert view != null;
    view.addTextChangedListener(new TextWatcher()
    {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after)
      {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count)
      {
      }

      @Override
      public void afterTextChanged(Editable s)
      {
        if (mNumbersCount != null)
        {
          mNumbersCount.setText(String.valueOf(s.length()));
        }
      }
    });

    view.setOnKeyListener(new View.OnKeyListener()
    {
      @Override
      public boolean onKey(View v, int keyCode, KeyEvent event)
      {
        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER) && isCalculating())
        {
          startOrCancelCalculation();
          return true;
        }

        return false;
      }
    });
    String lastInput = Preferences.getString(MainActivity.this, Preferences.LAST_NUMBER);
    if (!lastInput.isEmpty())
    {
      view.setText(lastInput);
      mNumbersCount.setText(String.valueOf(lastInput.length()));
    }
  }


  private void startOrCancelCalculation()
  {
    if (!isCalculating())
    {
      String inputNumberStr = mInput.getText().toString();

      if (inputNumberStr.isEmpty())
      {
        Toast.makeText(MainActivity.this, R.string.toast_enter_number, Toast.LENGTH_SHORT).show();
        return;
      }

      if (!NumberTypeFactory.checkInputNumber(inputNumberStr, NumberTypeFactory.MAX_PROGRAMM_SUPPORT_NUMBER))
      {
        Toast.makeText(MainActivity.this, R.string.toast_to_big_value, Toast.LENGTH_SHORT).show();
        return;
      }

      Preferences.setString(MainActivity.this, Preferences.LAST_NUMBER, inputNumberStr);
      prepareForStart();
      Intent runService = new Intent(this, JobService.class);
      runService.setAction(getAction());
      runService.putExtra(JobService.NUMBER, inputNumberStr);
      startService(runService);
      serviceHelper.bindService(JobService.class);
    }
    else
    {
      serviceHelper.sendMsgToService(JobService.MSG_CANCEL_JOB, null);
    }
  }

  private void prepareForStart()
  {
    mInput.setEnabled(false);
    mButtonFactorize.setText(R.string.btn_cancel);
    setIsCalculating(true);
    mResult.setVisibility(View.GONE);
    mProgressView.setVisibility(View.VISIBLE);
    setProgressValue(0);
  }

  private void finishedCalculation()
  {
    mButtonFactorize.setText(R.string.btn_factorize);
    mProgressView.setVisibility(View.GONE);
    mResult.setText(Preferences.getString(MainActivity.this, Preferences.LAST_RESULT));
    mInput.setEnabled(true);
    setIsCalculating(false);
  }

  private void setProgressValue(int value)
  {
    mProgressBar.setProgress(value);
    mProgressPercent.setText(String.valueOf(value) + "%");
  }

  private void showProgress(int value)
  {
    setProgressValue(value);
  }

  private void showResult(String value)
  {
    Preferences.setString(MainActivity.this, Preferences.LAST_RESULT, value);
    mProgressView.setVisibility(View.GONE);
    mResult.setText(value);
    mResult.setVisibility(View.VISIBLE);
  }

  private void showError(String value)
  {
    mProgressView.setVisibility(View.GONE);
    mResult.setVisibility(View.GONE);
    Toast.makeText(this, R.string.toast_error_during_calc, Toast.LENGTH_SHORT).show();
    Log.d(getClass().getName(), value);
  }

  private void showCanceled()
  {
    Preferences.setString(this, Preferences.LAST_RESULT, "");
    mResult.setText("");
  }

  private boolean isCalculating()
  {
    return Preferences.isCalculating(this);
  }

  private void setIsCalculating(boolean value)
  {
    Preferences.setCalculating(this, value);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState)
  {
    super.onSaveInstanceState(outState);
    outState.putInt(B_PROGRESS, mProgressBar.getProgress());
  }

  public String getAction()
  {
    if (rbJava.isChecked())
      return JobService.ACTION_JAVA;
    else
      return JobService.ACTION_CPP;
  }
}