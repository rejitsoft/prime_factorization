package com.rejitsoft.prime_factorization;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences
{
  public static final String IS_CALCULATING = "PREF_IS_CALCULATING";
  public static final String LAST_RESULT = "LAST_RESULT";
  public static final String LAST_NUMBER = "LAST_NUMBER";

  public static boolean isCalculating(Context context)
  {
    boolean result = false;
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    if (pref != null)
    {
      result = pref.getBoolean(IS_CALCULATING, false);
    }

    return result;
  }

  public static void setCalculating(Context context, boolean value)
  {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    if (pref != null)
    {
      SharedPreferences.Editor editPref =  pref.edit();
      editPref.putBoolean(IS_CALCULATING, value);
      editPref.commit();
    }
  }

  public static String getString(Context context, String KEY)
  {
    String result = "";
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    if (pref != null)
    {
      result = pref.getString(KEY, "");
    }

    return result;
  }

  public static void setString(Context context, String KEY, String result)
  {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    if (pref != null)
    {
      SharedPreferences.Editor editPref =  pref.edit();
      editPref.putString(KEY, result);
      editPref.commit();
    }

  }
}
