#include "com_rejitsoft_prime_factorization_NativePrimeFactorization.h"

#include <android/log.h>
#include <vector>

#include "bigInt.h"
#define Bigint BigInt::Rossi

#define  LOG_TAG    "libnative_prime_factors"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

/* Set to 1 to enable debug log traces. */
#define DEBUG 1

JNIEXPORT jobjectArray JNICALL Java_com_rejitsoft_prime_1factorization_NativePrimeFactorization_primeFactors
  (JNIEnv *env, jclass, jstring inputNumber, jobject callback)
{

  jboolean blnIsCopy;
  const char* nativeString = env->GetStringUTFChars(inputNumber , &blnIsCopy);
  jclass cls = env->GetObjectClass(callback);
  jmethodID onSuccess = env->GetMethodID(cls, "onSuccess", "([Ljava/lang/String;)V");
  jmethodID isCanceled = env->GetMethodID(cls, "isCanceled", "()Z");
  jmethodID onProgress = env->GetMethodID(cls, "onProgress", "(II)V");
  jmethodID onCancel = env->GetMethodID(cls, "onCancel", "()V");

  LOGI("primeFactors new");

  Bigint n(std::string(nativeString), BigInt::DEC_DIGIT);
  Bigint ONE = Bigint("1", BigInt::DEC_DIGIT);
  Bigint TWO = Bigint("2", BigInt::DEC_DIGIT);
  Bigint BN_TOTAL_PROGRESS = Bigint("100", BigInt::DEC_DIGIT);
  std::vector<Bigint> factors;
  int progress = 0;
  int last_progress = 0;
  Bigint i = TWO;
  Bigint limit = n / i;
  Bigint one_percent;
  for (; i <= limit ; i = i + ONE)
  {
    if (env->CallBooleanMethod(callback,isCanceled))
    {
      env->CallVoidMethod(callback, onCancel);
      return NULL;
    }

    limit = n / i;
    one_percent = ((limit > BN_TOTAL_PROGRESS) ? limit / BN_TOTAL_PROGRESS : ONE);
    progress = (int) (i / one_percent).toUnit();
    if (last_progress < progress)
    {
      last_progress = progress;
      env->CallVoidMethod(callback, onProgress, progress, 100);
    }

    while ( (n % i).toUnit() == 0)
    {
      factors.push_back(i);
      n = n / i;

      limit = n/i;
      one_percent = limit > BN_TOTAL_PROGRESS ? limit/BN_TOTAL_PROGRESS : ONE;
      if (n >= i && last_progress < progress)
      {
        progress = (int) (i/ one_percent).toUnit();
        last_progress = progress;
        env->CallVoidMethod(callback, onProgress, progress, 100);
      }
    }
  }

  if (n > ONE)
  {
    factors.push_back(n);
  }

  if (progress != 100)
    env->CallVoidMethod(callback, onProgress, 100, 100);

  jobjectArray ret;

  ret = (jobjectArray)env->NewObjectArray(factors.size() ,env->FindClass("java/lang/String"),env->NewStringUTF(""));
  for(int j=0; j<factors.size(); j++)
  {
    env->SetObjectArrayElement(ret, j ,env->NewStringUTF(factors[j].toStrDec().c_str()));
  }

  env->CallVoidMethod(callback, onSuccess, ret);
  env->ReleaseStringUTFChars(inputNumber, nativeString);
  return(ret);
}

jint JNI_OnLoad(JavaVM* aVm, void* aReserved)
{
  LOGI("Load ok");
  return JNI_VERSION_1_6;
}