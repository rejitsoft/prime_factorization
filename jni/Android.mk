LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := native_prime_factors
LOCAL_SRC_FILES := native_prime_factors.cpp bigInt.cpp
LOCAL_LDLIBS    := -llog
LOCAL_CPPFLAGS += -fexceptions

include $(BUILD_SHARED_LIBRARY)
